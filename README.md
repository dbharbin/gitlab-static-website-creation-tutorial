# GitLab Static Website creation tutorial

This tutorial walks through how to create a static website on GitLab and then assign a domain name from GoDaddy to the new website.
The website I own is sentientjourney.com

# Create Static Website from Pages template 

**Fork the demo repo and remove fork relationship** 

We will use the jekyll demo repo found [**here**](https://gitlab.com/pages)

<img src=videos/ForkAndRemoveForkRelationship.gif width=40% height=50% />


**Verify shared runners are enabled to be able to build the website** 

<img src=videos/CheckSharedRunners.gif width=40% height=50% />

If not already enabled enable the feature


**Configure and test the new website**

Must modify the a file to trigger a Pages build. Will edit and save README.  Can then check to see it building.

<img src=videos/ModifyToTriggerBuild.gif width=40% height=50% />


* **Note:** It can take 15-30 minutes for the website to propigate and become available.  Also may need to clear local cache if not rendering.


# Rename to my local project name and default GitLab url
First rename the local project and the GitLab project url name. 
I want it to be sentientjourney.com.

<img src=videos/RenameSite.gif width=40% height=50% />

Also update the _config.yml to point to the new name

<img src=videos/UpdateYml.gif width=40% height=50% />




# Redirect domain name from GoDaddy to point to the new website



